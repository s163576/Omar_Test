Feature: hello service

  Scenario: hello service retuns correct answer
    When I call the hello service
    Then I get the answer "Hello from RESTEasy Reactive"


  Scenario: hello and welcome service returns and correct answer
    When I call the hello service and omar
    Then I get the answer from server "Hello Omar"



  Scenario: Successful Payment
    Given the customer "Ryan" "Anderson" with CPR "061100-7124" has a bank account
    And the balance of that account is 1000
    And the customer is registered with DTUPay
    And the merchant "Jo" "Kuckles" with CPR number "110561-2741" has a bank account
    And the balance of that account is 2000
    And the merchant is registered with DTUPay
    When the merchant initiates a payment for "10" kr by the customer
    Then the payment is successful
    And the balance of the customer at the bank is 990 kr
    And the balance of the merchant at the bank is 2010 kr