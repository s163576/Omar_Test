package StepDefinitions;

import com.example.ExampleResource;
import com.example.Example_2;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testng.AssertJUnit.assertEquals;
import static org.wildfly.common.Assert.assertFalse;

public class Steps {

    String result,result2;
    Account customer;
    String customerID;
    boolean successful;
    DTUPay dtuPay=new DTUPay();
    ExampleResource service = new ExampleResource();
    @When("I call the hello service")
    public void iCallTheHelloService() {
        result = service.hello();
       // System.out.println(result);
    }
    @Then("I get the answer {string}")
    public void iGetTheAnswer(String string) {
        assertEquals(string,result);
    }


    /////////////////////////////////////////////////////////////77



        Example_2 example2 = new Example_2();
    @When("I call the hello service and omar")
    public void i_call_the_hello_service_and_omar() {
        // Write code here that turns the phrase above into concrete actions
        result2 = example2.helloOmar();
       // System.out.println(result2+"222222222222222222");
    }
    @Then("I get the answer from server {string}")
    public void i_get_the_answer_from_server(String string) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(string,result2);
    }


    //////////////////////////////////////77


    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void the_customer_with_cpr_has_a_bank_account(String string, String string2, String string3) {
        customer=new Account(string, string2, string3);

    }
    @Given("the balance of that account is {int}")
    public void the_balance_of_that_account_is(Integer int1) {
        customer.setBalance(BigDecimal.valueOf(int1));
        System.out.println(int1);
    }
    @Given("the customer is registered with DTUPay")
    public void the_customer_is_registered_with_dtu_pay() {
        customerID= dtuPay.registretAccount(customer);
        System.out.println(customerID);

    }
    @Given("the merchant {string} {string} with CPR number {string} has a bank account")
    public void the_merchant_with_cpr_number_has_a_bank_account(String string, String string2, String string3) {
        // Write code here that turns the phrase above into concrete actions


    }
    @Given("the merchant is registered with DTUPay")
    public void the_merchant_is_registered_with_dtu_pay() {
        // Write code here that turns the phrase above into concrete actions
    }
    @When("the merchant initiates a payment for {string} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(String string) {
        // Write code here that turns the phrase above into concrete actions

    }
    @Then("the payment is successful")
    public void the_payment_is_successful() {
        assertFalse(successful);

    }
    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        customer.getBalance();

    }
    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(Integer int1) {
        // Write code here that turns the phrase above into concrete actions


    }

}
