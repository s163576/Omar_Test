package dtu;

import dtu.model.Payment;

import java.util.ArrayList;
import java.util.List;

public class DTUPayment {
    public static DTUPayment Instance = new DTUPayment();
    List<Payment> paymentList;
    List<String> knownMerchants;
    List<String> knownClients;
    public DTUPayment() {
        this.paymentList = new ArrayList<>();
        this.knownClients = new ArrayList<>();
        this.knownClients.add("cid1");
        this.knownMerchants = new ArrayList<>();
        this.knownMerchants.add("mid1");
    }


    public int registerPayment(Payment pt) throws Exception {
        if(!knownClients.contains(pt.getCid()))
        {
            throw new Exception("customer with id " +pt.getCid()+" is unknown");
        }
        if(!knownMerchants.contains(pt.getMid()))
        {
            throw new Exception("merchant with id " +pt.getMid()+" is unknown");
        }
        this.paymentList.add(pt);
        return pt.getPaymentId();
    }

    public List<Payment> listPayments()
    {
        return paymentList;
    }
    public List<Payment> findPayment(String cid, String mid, int amount)
    {
        List<Payment> result = new ArrayList<>();
        for(Payment p : paymentList)
        {
            if(p.getCid().equals(cid) && p.getMid().equals(mid) && p.getAmount()==amount)
            {
                result.add(p);
            }
        }

        return result;
    }
}
