package dtu;


import dtu.model.Payment;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


@Path("paymentss")
public class PaymentsResource {

    DTUPayment dp = DTUPayment.Instance;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerPayment(Payment pt) throws Exception {
        try {
            int id = dp.registerPayment(pt);
            return Response.created(new URI("payments/"+id)).build();
        } catch (Exception e)
        {
            return Response.status(400).entity(e.getMessage())
                    .type("text/plain").build();
        }
    }

    /*@GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> findPayment(@QueryParam("cid") String cid, @QueryParam("mid") String mid, @QueryParam("amount") int amount)
    {
        return dp.findPayment(cid, mid, amount);
    }*/

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> listPayments()
    {
        return dp.listPayments();
    }

}
